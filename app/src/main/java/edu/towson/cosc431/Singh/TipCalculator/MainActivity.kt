package edu.towson.cosc431.Singh.TipCalculator

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        calculateButton.setOnClickListener {calculateTip()}
    }

    fun calculateTip() {
        val inputString = checkAmount.editableText.toString()
        val checkedId = radioGroup.checkedRadioButtonId

        // set the % on different checked id
        val customPercent = when(checkedId) {
            Radiobtn10.id -> 10.0
            Radiobtn20.id -> 20.0
            Radiobtn30.id -> 30.0
            else -> throw Exception("Unexpected value!")
        }

        // calculation
        val billamount = calculate(customPercent, inputString)

        // set the billamount textview
        when(billamount) {
            null -> resultTextView.text = "Error"
            else -> resultTextView.text = billamount
        }
    }

    fun calculate(customPercent: Double, strValue: String): String? {
        try {

            val billamount = strValue.toDouble()
            val customTip = (customPercent * billamount)/100
            val t_billamount = billamount + customTip

            return "Your calculated tip is $${String.format("%.2f", customTip)}" +
                    " and your total is $${String.format("%.2f", t_billamount)}"

        } catch (e: NumberFormatException) {
            return null
        }
    }

}
